import '../DTOs/transaction.dart';

class Transactions {
  // ========================= fields =========================

  static final List<Transaction> _transactions = [];

  // ========================= methods =========================

  static List<Transaction> getTransactions() => _transactions;

  static void addTransaction(Transaction transaction) {
    _transactions.add(transaction);
  }

  static void removeTransaction(String id) {
    _transactions.removeWhere((tx) => tx.id == id);
  }

  static String generateID() => DateTime.now().toString();
}
