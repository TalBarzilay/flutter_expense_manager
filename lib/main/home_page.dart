import 'dart:io';

import 'package:flutter/material.dart';

import '../DAL/transactions.dart';
import '../DTOs/transaction.dart';
import '../pages/expense_page.dart';
import '../widgets/add_transaction/add_transaction.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  void addTx(Transaction transaction) =>
      setState(() => Transactions.addTransaction(transaction));

  void removeTx(String id) =>
      setState(() => Transactions.removeTransaction(id));

  void _enterAddMode() => showModalBottomSheet(
      context: context, builder: (_) => AddTransaction(addTx));

  @override
  Widget build(BuildContext context) {
    final AppBar bar = AppBar(
      title: const Text('Expense Manager'),
      actions: [
        IconButton(
          onPressed: _enterAddMode,
          icon: const Icon(Icons.add),
        )
      ],
    );

    return Scaffold(
      appBar: bar,
      body: ExpensePage(
        Transactions.getTransactions(),
        removeTx,
        avilableHeight: MediaQuery.of(context).size.height - // total height
            MediaQuery.of(context).padding.top - // app bar height
            bar.preferredSize.height, // device bar height
      ),
      floatingActionButton: Platform.isIOS
          ? null
          : FloatingActionButton(
              child: const Icon(Icons.add), onPressed: _enterAddMode),
      floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
    );
  }
}
