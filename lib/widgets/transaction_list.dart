import 'package:expense_manager/widgets/no_transactions.dart';
import 'package:flutter/material.dart';

import '../DTOs/transaction.dart';
import 'transaction-box/tranaction_widget.dart';

class TransactionList extends StatelessWidget {
  final List<Transaction> transactions;
  final void Function(String) removeTransaction;

  const TransactionList(this.transactions, this.removeTransaction, {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return transactions.isEmpty
        ? const NoTransactions()
        : ListView.builder(
            itemBuilder: (BuildContext context, int index) => TransactionWidget(
              transactions[index],
              () => removeTransaction(transactions[index].id),
              key: ValueKey(transactions[index].id),
            ),
            itemCount: transactions.length,
          );
  }
}
