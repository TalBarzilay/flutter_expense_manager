import 'package:flutter/material.dart';

import './day_chart_item.dart';

class ExpenseChart extends StatelessWidget {
  final List<DayChartItem> bars;

  const ExpenseChart(this.bars, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Card(
        elevation: 10,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 20),
          child: Row(
            children: bars,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
          ),
        ),
      ),
      width: double.infinity,
      margin: const EdgeInsets.all(10),
    );
  }
}
