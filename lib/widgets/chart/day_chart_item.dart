import 'package:flutter/material.dart';

import '../../widgets/chart/chart_bar.dart';

class DayChartItem extends StatelessWidget {
  final double daySpending;
  final double totalWeekSpending;
  final String day;

  const DayChartItem(this.daySpending, this.totalWeekSpending, this.day,
      {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Flexible(
      fit: FlexFit.tight,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SizedBox(
            height: 20,
            child: FittedBox(
              child: Text('\$${daySpending.toStringAsFixed(0)}'),
            ),
          ),
          ChartBar(
            totalWeekSpending == 0 ? 0 : daySpending / totalWeekSpending,
          ),
          Text(day),
        ],
      ),
    );
  }
}
