import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../DAL/transactions.dart';
import '../../DTOs/transaction.dart';

class AddTransaction extends StatefulWidget {
  final void Function(Transaction transaction) _add;

  const AddTransaction(this._add, {Key? key}) : super(key: key);

  @override
  State<AddTransaction> createState() => _AddTransactionState();
}

class _AddTransactionState extends State<AddTransaction> {
  final _titleController = TextEditingController();
  final _annountController = TextEditingController();

  DateTime? _selectedDate;

  void _dosplayDatePicker() => showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(2021),
        lastDate: DateTime.now(),
      ).then(
        (date) {
          if (date == null) {
            return;
          }

          setState(() => _selectedDate = date);
        },
      );

  void _addTx() {
    final String title = _titleController.text;
    final double? ammount = double.tryParse(_annountController.text);
    final String id = Transactions.generateID();

    if (title.isEmpty || ammount! < 0 || _selectedDate == null) {
      return;
    }

    widget._add(Transaction(
      id: id,
      title: title,
      ammount: ammount,
      date: _selectedDate!,
    ));

    //remove the top layer - the module sheet
    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Card(
        elevation: 5,
        margin: EdgeInsets.only(
          left: 10,
          right: 10,
          top: 10,
          bottom: MediaQuery.of(context).viewInsets.bottom + 10,
        ),
        child: Column(children: [
          TextField(
            decoration: const InputDecoration(labelText: 'Title'),
            controller: _titleController,
            onSubmitted: (_) => _addTx(),
          ),
          TextField(
            decoration: const InputDecoration(labelText: 'Amount'),
            controller: _annountController,
            onSubmitted: (_) => _addTx,
            keyboardType: const TextInputType.numberWithOptions(decimal: true),
          ),
          SizedBox(
            height: 80,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    _selectedDate == null
                        ? 'please select date'
                        : DateFormat.yMd().format(_selectedDate!),
                  ),
                  TextButton(
                      onPressed: _dosplayDatePicker,
                      child: const Text('Choose...'))
                ],
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: ElevatedButton(
                  onPressed: _addTx,
                  child: const Text('Add Transaction'),
                  style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all(Colors.accents[3]),
                    foregroundColor:
                        MaterialStateProperty.all(Colors.accents[8]),
                  ),
                ),
              ),
            ],
          )
        ]),
      ),
    );
  }
}
