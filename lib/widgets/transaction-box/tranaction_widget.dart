import 'package:flutter/material.dart';

import '../../DTOs/transaction.dart';
import './transaction_price.dart';
import './transaction_description.dart';
import './transaction_remove.dart';

class TransactionWidget extends StatelessWidget {
  final Transaction transaction;
  final void Function() removeTransaction;

  const TransactionWidget(this.transaction, this.removeTransaction, {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Card(
        child: Row(
          children: [
            TransactionPrice(transaction.ammount),
            TransactionDescription(transaction.title, transaction.date),
            RemoveTransaction(removeTransaction),
          ],
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
        ),
        color: Theme.of(context).colorScheme.primary,
      ),
      width: double.infinity,
      margin: const EdgeInsets.symmetric(vertical: 5, horizontal: 30),
    );
  }
}
