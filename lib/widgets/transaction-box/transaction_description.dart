import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class TransactionDescription extends StatelessWidget {
  final String title;
  final DateTime date;

  const TransactionDescription(this.title, this.date, {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Text(
            title,
            style: TextStyle(
              color: Colors.accents[13],
              fontWeight: FontWeight.bold,
              fontSize: 20,
            ),
          ),
          Text(
            DateFormat.yMMMd().format(date),
            style: const TextStyle(
              color: Colors.blueGrey,
              fontSize: 15,
            ),
          ),
        ],
        crossAxisAlignment: CrossAxisAlignment.start,
      ),
      margin: const EdgeInsets.symmetric(horizontal: 15),
    );
  }
}
