import 'package:flutter/material.dart';

class TransactionPrice extends StatelessWidget {
  final double ammount;

  const TransactionPrice(this.ammount, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text(
        '\$${ammount.toStringAsFixed(2)}',
        style: TextStyle(
          color: Colors.accents[3],
          fontWeight: FontWeight.bold,
          fontSize: 25,
        ),
      ),
      margin: const EdgeInsets.symmetric(
        horizontal: 15,
        vertical: 10,
      ),
      decoration: BoxDecoration(
        border: Border.all(
          color: Colors.accents[3],
          width: 2,
        ),
      ),
      padding: const EdgeInsets.all(10),
    );
  }
}
