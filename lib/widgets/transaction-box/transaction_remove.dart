import 'package:flutter/material.dart';

class RemoveTransaction extends StatelessWidget {
  final void Function() remove;

  const RemoveTransaction(this.remove, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: remove,
      icon: const Icon(Icons.delete),
      color: Theme.of(context).errorColor,
    );
  }
}
