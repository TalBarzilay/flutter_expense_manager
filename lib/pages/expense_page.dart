import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../DTOs/transaction.dart';
import '../widgets/chart/day_chart_item.dart';
import '../widgets/chart/expense_chart.dart';
import '../widgets/transaction_list.dart';

class ExpensePage extends StatelessWidget {
  final List<Transaction> _transactions;
  final void Function(String) removeTransaction;
  final double avilableHeight;

  const ExpensePage(this._transactions, this.removeTransaction,
      {Key? key, required this.avilableHeight})
      : super(key: key);

  bool _isSameDate(DateTime date1, DateTime date2) =>
      (date1.year == date2.year &&
          date1.month == date2.month &&
          date1.day == date2.day);

  DateTime _dayOfWeek(index) => DateTime.now().subtract(
        Duration(days: index),
      );

  List<Transaction> _recentTransactions(int days) => _transactions
      .where((Transaction tx) =>
          tx.date.isAfter(DateTime.now().subtract(Duration(days: days))))
      .toList();

  List<Transaction> _transactionsOfDay(
          List<Transaction> transactions, DateTime day) =>
      transactions
          .where((Transaction tx) => _isSameDate(tx.date, day))
          .toList();

  double _totalExpensesOfDay(List<Transaction> transactions, DateTime day) =>
      _transactionsOfDay(transactions, day).fold(
          0,
          (double previousValue, Transaction element) =>
              previousValue + element.ammount);

  List<Map<String, Object>> _calcChartValues() => List.generate(7, (index) {
        final DateTime day = _dayOfWeek(index);
        final expenses = _totalExpensesOfDay(_recentTransactions(10), day);

        return {'day': DateFormat.E().format(day), 'expenses': expenses};
      });

  double _totalWeeklyExpenses(List<Map<String, Object>> info) => info.fold(
        0.0,
        (double previousValue, Map<String, Object> element) =>
            previousValue + (element['expenses'] as double),
      );

  List<DayChartItem> get _getDayCharts {
    final List<Map<String, Object>> info = _calcChartValues();
    final double total = _totalWeeklyExpenses(info);

    return _calcChartValues()
        .map((Map<String, Object> info) => DayChartItem(
              info['expenses'] as double,
              total,
              info['day'] as String,
            ))
        .toList()
        .reversed
        .toList();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: avilableHeight * 0.3,
          child: ExpenseChart(_getDayCharts),
        ),
        SizedBox(
          height: avilableHeight * 0.7,
          child: TransactionList(_transactions, removeTransaction),
        ),
      ],
    );
  }
}
