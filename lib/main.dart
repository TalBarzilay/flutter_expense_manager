import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'main/expense_app.dart';

void main() => _runMain();

// ============================== run main ==============================

void _runMain() {
  _ensurePortrait();

  runApp(const ExpenseApp());
}

// ============================= private methods =============================

void _ensurePortrait() => _ensureOrientation([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

void _ensureOrientation(List<DeviceOrientation> orientations) {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations(orientations);
}
